import $ from 'jquery';
import service from './service';

$( document ).ready(function() {
  $('#search-button').click(function() {
    var value = $('#search-input').val();
    $("#results").empty();
    const printWarning = service(value,$.ajax,populateSearchResults);
    if (printWarning){
      var resultRow = '<div class="result-row">Please use at least 3 characters for the search</div>';
      $("#results").append(resultRow);
    }
  });
});

var populateSearchResults = function(resultsData) {
  if(resultsData.length > 0){
    $.each(resultsData,function(key,value){
        var resultRow = '<div class="result-row">';
        resultRow += '<div class="result-name"><b>Name:</b>'+value.name+'</div>';
        resultRow += '<div class="result-mail"><b>Email:</b>'+value.email+'</div>';
        resultRow += '<div class="result-body"><b>Body:</b>'+value.body.substring(0, 64)+'</div>';
        resultRow += '</div>';
        $("#results").append(resultRow);
   });
  } else {
    var resultRow = '<div class="result-row">No results found</div>';
    $("#results").append(resultRow);
  }
};
