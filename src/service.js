function callService(value,xhr,onSuccess){
  var url = "https://jsonplaceholder.typicode.com/comments?q="+value+"&_end=20";
  if(value.length >= 3){
    xhr ({ 
        url: url,
        type: 'get',
        dataType: 'JSON',
        success: function(data)
        {
          onSuccess(data);
        }
    });
  } else {
    return true;
  }
};

module.exports = callService;