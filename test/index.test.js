const service = require('../src/service');

test('make call when value is more than 3 characters', () => {
  const mockXhr = jest.fn();
  service('lorem',mockXhr);
  expect(mockXhr.mock.calls.length).toBe(1);
});

test("don't make call when value is less than 3 characters", () => {
  const mockXhr = jest.fn();
  service('lo',mockXhr);
  expect(mockXhr.mock.calls.length).toBe(0);
});

test('expect xhr url to be the one desired', () => {
  const mockXhr = jest.fn();
  service('lorem',mockXhr);
  expect(mockXhr.mock.calls[0][0].url).toBe("https://jsonplaceholder.typicode.com/comments?q=lorem&_end=20");
});